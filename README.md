# PROYECTO FINAL Tech University - Perú (2º edición 2020)

Este API REST sera desplegado de manera local.

Este API REST esta diseñado para el Proyecto Final para la 2º edición 2020 del Tech University desarrollado de manera remota Perú-España.

Todo el código esta alojado en Bitbucket en los siguientes repositorios:

-  [Backend](https://bitbucket.org/renzorojasm/backend_ubuntu/src/master/)

-  [FrontEnd](https://bitbucket.org/renzorojasm/front-techu/src/master/src/)

Integrantes:

- Renzo Rojas Marquez

- Nilda Echevarria Meza


## Introducción
Esta aplicaciòn es un servicio web que modela una aplicación de banca móvil (webapp) en el que tendremos los recursos `Gestor de Cuentas` que contendrá listado de cuentas, alta de cuenta, inactivar cuenta, listar movimientos, detalle de movimientos, insertar movimientos(Deposito); así como el `Gestor de usuario` (cliente) que contendrá listado de usuarios, alta de usuario, eliminar usuario, actualizar usuario.

## Casos de Uso
```
$ GET
```
- `api/users` -> Devuelve el listado de clientes registrados.

- `api/users/:id` --> Devuelve el detalle de un cliente segùn el id que se busca.

- `api/users/:id/accounts` --> Devuelve el listado de cuentas de un cliente.

- `api/accounts/:id` --> Devuelve el listado de movimientos de una cuenta.

- `api/accounts/:id/movimientos/:id_mov` --> Devuelve el detalle de un movimientos de una cuenta.

```
$ POST
```
- `api/login` --> Login de un cliente, permite controlar el acceso individual del cliente al aplicativo.

- `api/logout` --> Logout de un cliente, permite la salida correcta del cliente del aplicativo.

- `api/users` --> Alta de un cliente, se añade un nuevo usuario a la base de datos.

- `api/accounts/` --> Alta de un movimiento, se añade un nuevo movimiento a la base de datos.

```
$ DELETE
```
- `api/usersDel/:api` --> Borramos un cliente de la base de datos.

- `api/accounts/:id/movimientos/:id_mov` --> Borramos un movimiento de la base de datos.

```
$ PUT
```
- `api/usersUpd/:id` --> Actualización de los datos de un cliente.

- `api/users/:id/account` --> Creación de cuentas de un cliente.(Funcionalidad adicional)

- `api/users/:id/accountInc/:idacc` --> Inactivar una cuentas de un cliente.(Funcionalidad adicional)

- `api/users/:id/account` --> Actualizar datos (saldo) de una cuenta - Deposito.(Funcionalidad adicional)


## Listado de comandos para la puesta en marcha
Para poner en marcha la aplicación tendremos que realizar los siguientes comandos.

-  `npm run prod` para la ejecución del backend.

-  `polymer serve` para le ejecución del frontend.

Para realizaciòn las pruebas unitarias usaremos el PostMan, esta aplicación permite realizar las peticiones necesarias para un API REST (GET, POST, DELETE, PUT).


## Recursos utilizados
```
$ BACKEND
```
`Node.js` Es un entorno JavaScript del lado del servidor basado en eventos. Tiene la responsabilidad de recibir las diferentes peticiones del front-end y ejecutar la lógica necesaria para llevar acabo los procesos de negocio. `Express` En el framework web minimalista, flexible, rápido, robusto y muy simple; escrito en JavaScript y alojado dentro del entorno de ejcución NodeJS.

Dependencias utilizadas:

-  "cors": "^2.8.5",

-  "dotenv": "^8.2.0",

-  "express": "^4.17.1",

-  "fs": "0.0.1-security",

-  "parse-int": "^1.0.3",

-  "request-json": "^0.6.5"

-  "nodemon": "^1.17.4"


```
$ BASE DE DATOS
```
`Mongo DB` Es una base de datos NoSQL orientado a documentos de código abierto. En lugar de guardar los datos en tablas, tal y como se hace en las bases de datos relacionados, MongoDB guarda estructuras de datos JSON con un esquema dinámico, haciendo que la integración de los datos en ciertas aplicaciones sea más fácil y rápida.

Las colecciones creadas para el proyecto son `users` que contiene la relaciòn clientes-cuentas y  `movimiento` que contiene los movimientos de una cuenta.


```
$ FRONTEND
```
`Polymer` Polymer es una biblioteca JavaScript de código abierto para la creación de aplicaciones web utilizando componentes web.

Dependencias utilizadas:

-  "polymer": "Polymer/polymer#^2.0.0",

-  "iron-ajax": "2.1.3",

-  "iron-pages": "2.1.1",

-  "app-router": "2.7.2"
